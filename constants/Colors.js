const tintColor = '#f1c40f';

export default {
  tintColor,
  tabIconDefault: '#eee',
  tabIconSelected: '#ecf0f1',
  tabBar: '#000000',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
