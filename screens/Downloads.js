import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    FlatList,
    WebView
} from 'react-native';
import {
    Title,
    ProgressBar,
    Card,
    Text,
    Chip,
    Divider,
    Snackbar,
    Button
} from 'react-native-paper';
import {
    FileSystem
} from 'expo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import {
    PulseIndicator
} from 'react-native-indicators';
import Colors from '../global/colors';
import Config from '../global/config';
import colors from '../global/colors';





export default class DownloadsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            rootUrl: Config.rootUrl,
            appId: Config.appId,
            appKey: Config.appKey,
            data: this.props.navigation.state.params,
            downloadProgress: null,
            downloadDone: false,
            videoURL: null,
            loader: true,
            downloadedObject: {
                object: {
                    title: null,
                    remoteLink: null,
                    localPath: null,
                    dateCreated: null
                }
            },
            downloadedArray: [],
            newObject: null,
            // link status is for keeping track of whether we got the real file link
            linkStatus: null,
        })
    }


    static navigationOptions = ({ navigation }) => ({
        title: 'Downloads',
        headerTransparent: false,
        headerTintColor: Colors.activeText.default,
        headerStyle: {
            elevation: 0,
            backgroundColor: Colors.secondary.default
        }
    });

    componentDidMount = async () => {
        this._refreshToken().then(() => {
            this._fetchDownloadedData();
        })
    }

    _refreshToken = async () => {
        const bearer = 'Bearer ' + this.state.token;
        try {
            const newToken = await fetch(this.state.rootUrl + '/api/refresh-token', {
                method: 'GET',
                headers: {
                    appId: this.state.appId,
                    appKey: this.state.appKey,
                    Authorization: bearer,
                }
            });
            const newTokenResponse = await newToken.json();
            AsyncStorage.setItem('logginKey', newTokenResponse.token);
            const value = await AsyncStorage.getItem('logginKey');
        } catch (e) {
            this.setState({
                loading: false,
                error: true,
                title: '404',
                message: 'Ooops.. no connection,Please logout and login again to continue',
                showConfirmButton: false
            })
        }
    }

    _fetchDownloadedData = async () => {
        const value = await AsyncStorage.getItem('downloadedData');
        const downloadedObject = JSON.parse(value);
        var array = [];
        for (var key in downloadedObject) {
            if (downloadedObject.hasOwnProperty(key)) {
                array.push(
                    { object: downloadedObject[key] }
                )
            }
        }
        this.setState({ downloadedArray: array })
        this.setState({
            loading: false
        })
        // if (this.state.data == undefined) {
        //     this.setState({
        //         loading: false,
        //         linkStatus: 'nothing to download'
        //     })
        // } else {
        //     this._ondownload();
        // }
    }

    _onplay = (item) => {
        this.props.navigation.navigate('LocalVideo', item);
    }


    _ondownload = async () => {
        var result = this.state.downloadedArray.find(element => {
            return element.object.title == this.state.data.title
        }
        )
        // alert(JSON.stringify(result));
        if (result == undefined) {
            token = await AsyncStorage.getItem('logginKey');
            var bearer = 'Bearer ' + token;
            const dataJson = await fetch(this.state.rootUrl + '/api/get-vimeo-id', {
                method: 'POST',
                withCredentials: true,
                credentials: 'include',
                headers: {
                    appId: this.state.appId,
                    appKey: this.state.appKey,
                    Authorization: bearer,
                    accept: 'application/json',
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    id: this.state.data.id
                })
            })
            var dataResponse = await dataJson.json();
            if (dataResponse.error == 0) {
                this.setState({
                    videoURL: dataResponse.data,
                    loader: false
                });
            }
            else {
                // alert('3' + JSON.stringify(dataResponse));
            }
        }
        else {
            this.setState({ downloadDone: true })
        }
    }

    // this function is triggered when the url in the webview changes
    _onNavigationStateChange = async webViewState => {
        this.setState({
            linkStatus: 'got the file'
        })
        try {
            //get progress
            const callback = downloadProgress => {
                const progress =
                    downloadProgress.totalBytesWritten /
                    downloadProgress.totalBytesExpectedToWrite;
                this.setState({
                    downloadProgress: progress,
                });
            };
            //videourl to be downloaded
            const downloadResumable = FileSystem.createDownloadResumable(
                webViewState.url,
                FileSystem.documentDirectory + this.state.data.title + '.mp4',
                {},
                callback
            );
            //downloading
            try {
                const { uri } = await downloadResumable.downloadAsync();
                this.setState({ downloadedObject: { object: { title: this.state.data.title, remoteLink: '', localPath: uri, dateCreated: new Date() } }, downloadDone: true })
                AsyncStorage.mergeItem('downloadedData', JSON.stringify(this.state.downloadedObject), () => {
                    AsyncStorage.getItem('downloadedData', (err, result) => {
                        const downloadedObject = JSON.parse(result);
                        var array = [];
                        for (var key in downloadedObject) {
                            if (downloadedObject.hasOwnProperty(key)) {
                                array.push(
                                    { object: downloadedObject[key] }
                                )
                            }
                        }
                        this.setState({ downloadedArray: [...this.state.downloadedArray, ...array] });
                        this.state.downloadedArray.forEach(element => {
                            this.state.newObject += element.object
                        });
                        // alert(JSON.stringify(this.state.newObject, null, 4))
                        AsyncStorage.mergeItem('downloadedData', JSON.stringify(this.state.newObject), () => { })
                    });
                });
            }
            catch (e) {
                // alert('<therererer>'+e);
            }
        }
        catch (e) {
            // alert('<herererere>'+e);
        }
    }

    render() {
        if (this.state.loader == true) {
            return (
                <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
                    <PulseIndicator animating={true} color={Colors.activeText.default} size={30} />
                </View>
            )
        } else {
            // if (this.state.downloadedArray.length == 0) {
            //     return (
            //         <View style={styles.container}>
            //             <Title style={{ color: colors.activeText.default }}>You don't have any downloaded Film</Title>
            //         </View>
            //     )
            // } else {
            //     return (
            //         <View style={styles.container}>
            //         </View>
            //     )
            // }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary.default,
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    downloadCard: {
        margin: 10,
        backgroundColor: Colors.secondary.default
    },
    emptyBox: {
        width: 100,
        height: 100
    }
});