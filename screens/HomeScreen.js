//dependencies
import React from 'react';
import {
    Dimensions,
    ScrollView,
    StyleSheet,
    View,
    FlatList,
    ImageBackground,
    RefreshControl,
    StatusBar,
    Animated,
    AsyncStorage,
    Image
} from 'react-native';
import {
    TouchableRipple,
    Text,
    Button,
    Headline,
    Badge
} from 'react-native-paper';
import {
    Right,
    Fab,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    PulseIndicator
} from 'react-native-indicators';
import { Updates } from 'expo';
//global components
import {
    fetchTopMovie,
    fetchFeaturedMovie,
    fetchtrendingMovies,
    fetchSeries,
    fetchRecentMovies,
    fetchMovies,
    fetchShortMovies,
    fetchBongoMovies
} from './requests';
import Colors from '../global/colors';
import Config from '../global/config';


const Logo = () => {
    return (
        <View style={{ width: Dimensions.get('window').width / 4 }}>
            <Image source={require('../assets/images/zacuheader.png')} style={{ width: '100%', height: '100%', margin: 5 }} resizeMode='contain' />
        </View>
    )
}


SCREEN_HEIGHT = Dimensions.get('window').height

export default class TvShow extends React.Component {

    //pageHeader
    static navigationOptions = (props) => ({
        // header:null,
        headerTitle: <Logo />,
        headerTransparent: true,
        headerTintColor: Colors.activeText.default,
        headerTitleStyle: { flex: 1, textAlign: 'center', justifyContent: 'center' },
        headerStyle: {
            backgroundColor: !props.navigation.state.params ? 'transparent' : props.navigation.state.params.headerbackgroundColor,
        },
    });

    //constructor
    constructor(props) {
        super(props);
        this.scrollY = new Animated.Value(0);
        this.state = ({
            rootUrl: Config.rootUrl,
            appId: Config.appId,
            appKey: Config.appKey,
            topMovieData: [],
            featuredMoviesData: [],
            popularMoviesData: [],
            recentMoviesData: [],
            bongoMovies: [],
            seriesData: [],
            moviesData: [],
            shortMoviesData: [],
            top_item: [],
            opacity: 1,
            fabActive: 'false',
            scrollY: new Animated.Value(0),
            loggedIn: null,
            refreshing: false,
            loader: true,
            showHeader: true,
            date: null,
            offSet: 0,
            limit: 50
        })
    }

    componentWillMount() {
        // this.interval = setInterval(this.fetchData,3600);
        this._updateCheck();
        this.fetchData();
        StatusBar.setHidden(false);
        this.props.navigation.setParams({
            headerbackgroundColor: this.scrollY.interpolate({
                inputRange: [0, SCREEN_HEIGHT / 2 - 40],
                outputRange: ['transparent', Colors.headerColorOnScroll.default],
                extrapolate: 'clamp',
            }),
        });
        this._loginCheck();
    }

    //check if the film is new
    _checkIfnew = (str) => {
        if (str == null) {
            return str = '';
        }
        else {
            var one_day = 1000 * 60 * 60 * 24;
            var q = new Date();
            var m = q.getMonth();
            var d = q.getDay();
            var y = q.getFullYear();
            var h = q.getHours();
            var min = q.getMinutes();
            var sec = q.getSeconds();
            var date = new Date(y, m, d, h, min, sec);
            var mydate = new Date(str.slice(0, 4), str.slice(5, 7), str.slice(8, 10), str.slice(11, 13), str.slice(14, 16), str.slice(17, 19));
            var difference = date - mydate;
            var days = Math.round(difference / one_day);
            if (days < 7) {
                return str = 'true';
            }
            else {
                return str = 'false';
            }
        }
    }

    //check for updates
    _updateCheck = async () => {
        try {
            const update = await Updates.checkForUpdateAsync();
            if (update.isAvailable) {
                await Updates.fetchUpdateAsync().then(() => Updates.reloadFromCache());
            };
        } catch (e) {
            console.log("Update fail", e);
        }
    }

    //checking if loggedIn
    _loginCheck = async () => {
        try {
            const value = await AsyncStorage.getItem('logginKey');
            if (value != 'loggedOut') {
                this.setState({ loggedIn: 'LoggedIn' })
            }
            else {
                this.setState({
                    showAlert: true
                });
            }
        } catch (error) {
            alert(error.message)
        }
    }

    // fetching data from the API
    fetchOnlineData = async () => {
        const topMovieResponse = await fetchTopMovie();
        const featuredMoviesResponse = await fetchFeaturedMovie();
        const popularMoviesResponse = await fetchtrendingMovies();
        const recentMoviesResponse = await fetchRecentMovies();
        const bongoMoviesResponse = await fetchBongoMovies();
        const seriesResponse = await fetchSeries();
        const moviesResponse = await fetchMovies();
        const shortMoviesResponse = await fetchShortMovies();
        await AsyncStorage.setItem('TopMovie',JSON.stringify(topMovieResponse));
        await AsyncStorage.setItem('FeaturedMovie',JSON.stringify(featuredMoviesResponse));
        await AsyncStorage.setItem('PopularMovie',JSON.stringify(popularMoviesResponse));
        await AsyncStorage.setItem('RecentMovie',JSON.stringify(recentMoviesResponse));
        await AsyncStorage.setItem('BongoMovie',JSON.stringify(bongoMoviesResponse));
        await AsyncStorage.setItem('Series',JSON.stringify(seriesResponse));
        await AsyncStorage.setItem('Movies',JSON.stringify(moviesResponse));
        await AsyncStorage.setItem('ShortMovies',JSON.stringify(shortMoviesResponse));
        await AsyncStorage.setItem('FirstLoad','not first load');
        await this.fetchLocalData();
    }

    fetchLocalData = async () => {
        const topMovieResponse = await AsyncStorage.getItem('TopMovie');
        const featuredMoviesResponse = await AsyncStorage.getItem('FeaturedMovie');
        const popularMoviesResponse = await AsyncStorage.getItem('PopularMovie');
        const recentMoviesResponse = await AsyncStorage.getItem('RecentMovie');
        const bongoMoviesResponse = await AsyncStorage.getItem('BongoMovie');
        const seriesResponse = await AsyncStorage.getItem('Series');
        const moviesResponse = await AsyncStorage.getItem('Movies');
        const shortMoviesResponse = await AsyncStorage.getItem('ShortMovies');
        this.setState({ topMovieData: JSON.parse(topMovieResponse) });
        this.setState({ featuredMoviesData: JSON.parse(featuredMoviesResponse) });
        this.setState({ popularMoviesData: JSON.parse(popularMoviesResponse) });
        this.setState({ recentMoviesData: JSON.parse(recentMoviesResponse) });
        this.setState({ bongoMovies: JSON.parse(bongoMoviesResponse) });
        this.setState({ seriesData: JSON.parse(seriesResponse) });
        this.setState({ moviesData: JSON.parse(moviesResponse) });
        this.setState({ shortMoviesData: JSON.parse(shortMoviesResponse) });
        this.setState({
            loader:false
        });
    }


    //fetching data
    fetchData = async () => {
        const firstTime = await AsyncStorage.getItem('FirstLoad');
        if (firstTime == null) {
            await this.fetchOnlineData();
        } else {
            await this.fetchLocalData();
            this.fetchOnlineData();
        }
        this.setState({ loader: false });
    }

    //fetching on refresh
    _onRefresh = async () => {
        this.setState({ refreshing: true },
            () => { this.componentDidMount() });
        this.setState({ refreshing: false });
    }

    //passing and navigating to details page
    _details = (data) => {
        this.props.navigation.navigate(data.category ? 'Details' : 'SeriesDetails', data)
    }


    render() {
        const FAB = (props) => {
            if (this.state.loggedIn == 'LoggedIn') {
                return (
                    <View></View>
                )
            }
            else {
                return (
                    <Fab
                        active={this.state.fabActive}
                        direction="up"
                        containerStyle={{}}
                        style={{ backgroundColor: '#5067FF' }}
                        position="bottomRight"
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Ionicons name="ios-add" />
                        <Button style={{ backgroundColor: '#34A34F' }} onPress={() => this.props.navigation.navigate('LoginPage')}>
                            <Ionicons name="ios-add" />
                        </Button>
                    </Fab>
                )
            }
        };
        if (this.state.loader == true) {
            return (
                <ImageBackground
                    source={require('../assets/images/imgbackground3.png')}
                    style={{ flexGrow: 1, backgroundColor: Colors.primary.default, alignItems: 'center', justifyContent: 'center' }}>
                    <PulseIndicator animating={true} color={'#ecf0f1'} size={30} />
                </ImageBackground>
            );
        }
        else {
            return (
                <View style={styles.container}>
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                            />
                        }

                        style={styles.container}
                        onScroll={Animated.event([
                            { nativeEvent: { contentOffset: { y: this.scrollY } } },
                        ])}
                        scrollEventThrottle={16}
                    >
                        <View style={styles.topContainer}>
                            <FlatList
                                data={this.state.topMovieData}
                                keyExtractor={(item) => 'item'}
                                renderItem={({ item }) =>
                                    <TouchableRipple onPress={() => this._details(item)}>
                                        <ImageBackground
                                            style={styles.topImageFull}
                                            source={{ uri: this.state.rootUrl + '/uploads/posts/' + item.mobile_cover }}
                                            resizeMode='stretch'
                                            imageStyle={{ width: Dimensions.get('window').width, height: '120%' }}
                                        // blurRadius={1}
                                        >
                                            <ImageBackground style={styles.topImageBackground} source={require('../assets/images/topback.png')} resizeMode='cover'>
                                                <View style={styles.fac}>
                                                    <View style={[{ justifyContent: 'center', alignItems: 'center' }]}>
                                                        <Headline style={{ color: Colors.activeText.default }}>{`${item.title}`}</Headline>
                                                        <Button style={{ backgroundColor: Colors.activeText.default, marginTop: 5 }} onPress={() => this._details(item)}>
                                                            <Text style={{ color: Colors.activeTextDark.default }}>Watch now</Text>
                                                        </Button>
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </ImageBackground>
                                    </TouchableRipple>
                                }
                            />
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Featured Movies
                                </Text>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.featuredMoviesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Popular Movies
                                </Text>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.popularMoviesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Recent Added
                                </Text>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.recentMoviesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Bongo Movies
                                </Text>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.bongoMovies}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Best Series
                                </Text>
                                <Right>
                                    <TouchableRipple onPress={() => this.props.navigation.navigate('TvShow')}>
                                        <Ionicons style={[{ marginRight: 10 }]} name='ios-more' size={40} color='rgb(95,95,102)' />
                                    </TouchableRipple>
                                </Right>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.seriesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Best Movies
                                </Text>
                                <Right>
                                    <TouchableRipple onPress={() => this.props.navigation.navigate('Movie')}>
                                        <Ionicons style={[{ marginRight: 10 }]} name='ios-more' size={40} color='rgb(95,95,102)' />
                                    </TouchableRipple>
                                </Right>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.moviesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    imageStyle={{ borderRadius: 10 }}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleHeader}>
                                <Text style={styles.activeText}>
                                    Best Short Movies
                                </Text>
                                <Right>
                                    <TouchableRipple onPress={() => this.props.navigation.navigate('shortMovie')}>
                                        <Ionicons style={[{ marginRight: 10 }]} name='ios-more' size={40} color='rgb(95,95,102)' />
                                    </TouchableRipple>
                                </Right>
                            </View>
                            <View style={styles.listContainer}>
                                <FlatList
                                    horizontal
                                    style={styles.flatList}
                                    data={this.state.shortMoviesData}
                                    keyExtractor={({ item }) => 'key'}
                                    renderItem={({ item }) =>
                                        <TouchableRipple onPress={() => this._details(item)}>
                                            <View style={styles.movieCard}>
                                                <ImageBackground
                                                    style={styles.imageFull}
                                                    source={{ uri: this.state.rootUrl + '/uploads/posts/300/' + item.graphical_mobile_cover }}
                                                    imageStyle={{ borderRadius: 10 }}
                                                >
                                                    <Badge style={{ opacity: this._checkIfnew(item.published_at) == 'true' ? 1 : 0, position: 'absolute', bottom: 5, right: 5, backgroundColor: Colors.activeAccentText.default }} mode='flat'>
                                                        <Text style={{ color: Colors.activeText.default }}>New</Text>
                                                    </Badge>
                                                </ImageBackground>
                                            </View>
                                        </TouchableRipple>
                                    }
                                />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.primary.default
    },
    topContainer: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2 + 70,
        marginBottom: 5
    },
    topImageFull: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2 + 70,
        borderRadius: 10
    },
    topImageBackground: {
        backgroundColor: 'rgba(0,0,0,0.2)',
        width: '100%',
        height: '100%',
        marginBottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageFull: {
        width: '100%',
        height: '100%',
        borderRadius: 10
    },
    row: {
        flexDirection: 'row'
    },
    titleHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 7,
        marginTop: 5
    },
    activeText: {
        color: Colors.activeText.default,
        margin: 5
    },
    activeAccentText: {
        color: Colors.activeAccentText.default
    },
    flatList: {
        marginLeft: 10,
        marginBottom: 10
    },
    movieCard: {
        width: Dimensions.get('window').width / 2 - 70,
        height: Dimensions.get('window').height / 3 - 70,
        backgroundColor: '#f39c12',
        margin: 2.5,
        borderRadius: 10
    },
    cardContainer: {
        backgroundColor: Colors.primary.default,
        margin: 0,
        borderRadius: 10
    },
    fac: {
        position: 'absolute',
        margin: 15,
        bottom: 0,
    },
});
